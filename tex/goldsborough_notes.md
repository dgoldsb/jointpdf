# Workflow

* Every day, take one paper on the train
* Doel: op werk een planning maken voor mijn thesis
	* elke dag 1 paper on lonenn bag, vouw er een kladblaadje bij, leren jas aan, morfen ooj echt implementatie, dinsdag meeting quax
	weekend weten ezosys dataset

# To do

**Milestone 1**: Preparations

[ ] Read papers (0/17), make notes in seperate tex files
[ ] Formulate idea applying MI profile to ecology
[ ] Make an envelope of several random types of MI profiles, and record properties

**Milestone 2**: Research design

[ ] Come up with a research design

**Milestone 3**: Implementation

**Milestone 4**: Result analysis

**Milestone 5**: Write introduction/etc.

**Milestone 6**: Extend to real world

# Notes to include in paper

* X-or is perfect synergistic
* Addition is straight line in profile, if the ratios are similar (also when they are not I thnk)
* If the average profile is above the line, it means redundancy
* Betekenis geven aan het idee van synergy is het doel van dit paper

# Experimental design

* Make hypothesis maximum vs. gemiddelde different
* Hypothesis below the straight line to above possible, find a proof?
* Trein thesiswerk facilitaten, papers printen

# Rebecca advies

And I just started reading a bunch of different papers... you need to sort of get a large overview of all the sub topics with your main topic. But don't go for more then 5 or else it gets way to overwhelming... If you have at least one good paper you can start by read thier literature review, and see what kind of info they focused on and then go from there... Also look up the sources used by the literature you have
It sort of helps to make a big mind map and to see what you need to know about in order to answer your question... It's probably really different for your topic, but for me I couldn't make conclusion on my research unless I knew about the current theories in copyright research, piracy, success of other online product that use freemium pricing, and different online distribution models...
So I had a sort of wires mind map, connection all of those topics together in different ways and also listing the different paper I could find for each sub topic. Then I would also make a small summary of each paper and write down the main conclusion of the paper. And then I just those main conclusion guide me... Sometimes a conclusion would lead to more questions so I'd do more research. I'm trying to finish my list of references today and I think I have around 40ish sources for the literature review alone

# Quax advies

* Zie keep, google background research sessions
* Download Shannon and read
* Rick: voor boven de zeven, gefocuste literatuurstudie die laat zien waar de knowledge gaps zitten
* Voor paper natuurlijk vele compressed, daarom even in note-vorm houden for now
* Papers printen aan de hand van outline
* Planning voor mezelf maken
* Files aanmaken